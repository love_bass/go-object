package object

import (
	"sync"
	"testing"
	"time"
)

// TestNewTime 验证 NewTime 是否正确构造 Time 对象，并且 threadSafe 参数是否生效
func TestNewTime(t *testing.T) {
	t.Run("Test thread safety enabled", func(t *testing.T) {
		timeObj := NewTime(time.Now(), true)
		_, ok := timeObj.locker.(*timeSafeLocker)
		if !ok {
			t.Errorf("Expected timeSafeLocker, got %T", timeObj.locker)
		}
	})

	t.Run("Test thread safety disabled", func(t *testing.T) {
		timeObj := NewTime(time.Now(), false)
		_, ok := timeObj.locker.(timeNotLocker)
		if !ok {
			t.Errorf("Expected timeNotLocker, got %T", timeObj.locker)
		}
	})
}

// TestTime_Add 测试 Time 结构体的 Add 方法
func TestTime_Add(t *testing.T) {
	now := time.Now()
	timeObj := NewTime(now, true) // 使用线程安全模式

	duration := 10 * time.Second
	timeObj.Add(duration)

	expected := now.Add(duration)
	if !timeObj.Get().Equal(expected) {
		t.Errorf("Expected time %v, got %v", expected, timeObj.Get())
	}
}

// TestTime_ConcurrentAccess 测试 Time 结构体的并发访问安全性
func TestTime_ConcurrentAccess(t *testing.T) {
	timeObj := NewTime(time.Now(), true)

	wait := sync.WaitGroup{}
	for i := 0; i < 100; i++ {
		wait.Add(1)
		go func() {
			defer wait.Done()
			timeObj.Add(1 * time.Second) // 并发增加时间
		}()
	}

	wait.Wait()

	// 由于具体的时间增加操作依赖于协程的调度，这里不检查具体的时间值
	// 只确保代码没有发生并发冲突导致的panic等问题
}

// TestTime_BeforeAfter 测试 Before 和 After 方法
func TestTime_BeforeAfter(t *testing.T) {
	now := time.Now()
	past := now.Add(-10 * time.Second)  // 10秒前
	future := now.Add(10 * time.Second) // 10秒后

	timeObjNow := NewTime(now, false)
	timeObjPast := NewTime(past, false)
	timeObjFuture := NewTime(future, false)

	if !timeObjPast.Before(timeObjNow.Get()) {
		t.Errorf("Expected past to be before now")
	}

	if !timeObjFuture.After(timeObjNow.Get()) {
		t.Errorf("Expected future to be after now")
	}
}

// TestTime_Format 测试 Format 方法
func TestTime_Format(t *testing.T) {
	now := time.Now()
	timeObj := NewTime(now, false)

	expected := now.Format("2006-01-02 15:04:05")
	formatted := timeObj.Format("2006-01-02 15:04:05")

	if formatted != expected {
		t.Errorf("Expected formatted time %s, got %s", expected, formatted)
	}
}

// TestTime_InLocalUTC 测试 In, Local 和 UTC 方法
func TestTime_InLocalUTC(t *testing.T) {
	now := time.Now()
	timeObj := NewTime(now, false)

	// 测试 In 方法
	loc, _ := time.LoadLocation("Europe/Paris")
	timeInParis := timeObj.In(loc)
	if timeInParis.Location() != loc {
		t.Errorf("Expected location %v, got %v", loc, timeInParis.Location())
	}

	// 测试 Local 方法
	timeLocal := timeObj.Local()
	if timeLocal.Location() != time.Local {
		t.Errorf("Expected location %v, got %v", time.Local, timeLocal.Location())
	}

	// 测试 UTC 方法
	timeUTC := timeObj.UTC()
	if timeUTC.Location() != time.UTC {
		t.Errorf("Expected location %v, got %v", time.UTC, timeUTC.Location())
	}
}

// TestTime_SetAndGet 测试 Set 和 Get 方法
func TestTime_SetAndGet(t *testing.T) {
	now := time.Now()
	timeObj := NewTime(now, true) // 使用线程安全模式

	newTime := now.Add(5 * time.Hour)
	timeObj.Set(newTime)

	if !timeObj.Get().Equal(newTime) {
		t.Errorf("Expected time %v, got %v", newTime, timeObj.Get())
	}
}

// TestTime_MarshalUnmarshal 测试 encoding 相关的方法
func TestTime_MarshalUnmarshal(t *testing.T) {
	now := time.Now()
	timeObj := NewTime(now, false)

	// 测试 MarshalBinary 方法
	binaryData, err := timeObj.MarshalBinary()
	if err != nil {
		t.Errorf("Error marshaling time to binary: %s", err)
	}

	// 测试 UnmarshalBinary 方法
	newTimeObj := NewTime(time.Time{}, false)
	err = newTimeObj.UnmarshalBinary(binaryData)
	if err != nil {
		t.Errorf("Error unmarshaling time from binary: %s", err)
	}
	if !newTimeObj.Get().Equal(now) {
		t.Errorf("Expected time %v after unmarshal, got %v", now, newTimeObj.Get())
	}

	// 测试 MarshalJSON 方法
	jsonData, err := timeObj.MarshalJSON()
	if err != nil {
		t.Errorf("Error marshaling time to JSON: %s", err)
	}

	// 测试 UnmarshalJSON 方法
	err = newTimeObj.UnmarshalJSON(jsonData)
	if err != nil {
		t.Errorf("Error unmarshaling time from JSON: %s", err)
	}
	if !newTimeObj.Get().Equal(now) {
		t.Errorf("Expected time %v after unmarshal, got %v", now, newTimeObj.Get())
	}

	// 测试 MarshalText 方法
	textData, err := timeObj.MarshalText()
	if err != nil {
		t.Errorf("Error marshaling time to text: %s", err)
	}

	// 测试 UnmarshalText 方法
	err = newTimeObj.UnmarshalText(textData)
	if err != nil {
		t.Errorf("Error unmarshaling time from text: %s", err)
	}
	if !newTimeObj.Get().Equal(now) {
		t.Errorf("Expected time %v after unmarshal, got %v", now, newTimeObj.Get())
	}
}

// TestTime_ClockDateDay 等测试 Clock, Date, Day 等方法
func TestTime_ClockDateDay(t *testing.T) {
	now := time.Now()
	timeObj := NewTime(now, false)

	// 测试 Clock 方法
	hour, min, sec := timeObj.Clock()
	if hour != now.Hour() || min != now.Minute() || sec != now.Second() {
		t.Errorf("Clock() returned incorrect values")
	}

	// 测试 Date 方法
	year, month, day := timeObj.Date()
	if year != now.Year() || month != now.Month() || day != now.Day() {
		t.Errorf("Date() returned incorrect values")
	}

	// 测试 Day 方法
	dayReturned := timeObj.Day()
	if dayReturned != now.Day() {
		t.Errorf("Day() returned incorrect value")
	}
}

// TestTime_WeekdayYearDayISOWeek 测试 Weekday, YearDay, ISOWeek 等方法
func TestTime_WeekdayYearDayISOWeek(t *testing.T) {
	now := time.Now()
	timeObj := NewTime(now, false)

	// 测试 Weekday 方法
	weekday := timeObj.Weekday()
	if weekday != now.Weekday() {
		t.Errorf("Weekday() returned incorrect value")
	}

	// 测试 YearDay 方法
	yearDay := timeObj.YearDay()
	if yearDay != now.YearDay() {
		t.Errorf("YearDay() returned incorrect value")
	}

	// 测试 ISOWeek 方法
	yearISO, weekISO := timeObj.ISOWeek()
	y, w := now.ISOWeek()
	if yearISO != y || weekISO != w {
		t.Errorf("ISOWeek() returned incorrect values")
	}
}

// TestTime_ZoneUnix 等测试 Zone, Unix, UnixNano 等方法
func TestTime_ZoneUnix(t *testing.T) {
	now := time.Now()
	timeObj := NewTime(now, false)

	// 测试 Zone 方法
	zone, offset := timeObj.Zone()
	z, o := now.Zone()
	if zone != z || offset != o {
		t.Errorf("Zone() returned incorrect values")
	}

	// 测试 Unix 方法
	unixTime := timeObj.Unix()
	if unixTime != now.Unix() {
		t.Errorf("Unix() returned incorrect value")
	}

	// 测试 UnixNano 方法
	unixNano := timeObj.UnixNano()
	if unixNano != now.UnixNano() {
		t.Errorf("UnixNano() returned incorrect value")
	}
}

// TestTime_SinceUntil 测试 Since 和 Until 方法
func TestTime_SinceUntil(t *testing.T) {
	past := time.Now().Add(-5 * time.Minute)
	future := time.Now().Add(5 * time.Minute)
	pastTimeObj := NewTime(past, false)
	futureTimeObj := NewTime(future, false)

	// 测试 Since 方法
	sinceDuration := pastTimeObj.Since()
	if sinceDuration < 0 {
		t.Errorf("Since() should return a positive duration for past times")
	}

	// 测试 Until 方法
	untilDuration := futureTimeObj.Until()
	if untilDuration < 0 {
		t.Errorf("Until() should return a positive duration for future times")
	}
}

// TestTime_EdgeCases 测试边界值和特殊日期
func TestTime_EdgeCases(t *testing.T) {
	// 测试零值
	zeroTimeObj := NewTime(time.Time{}, false)
	if !zeroTimeObj.IsZero() {
		t.Errorf("Expected zero time to be zero")
	}

	// 测试闰年
	leapYearTime := NewTime(time.Date(2020, 2, 29, 0, 0, 0, 0, time.UTC), false)
	nextDay := leapYearTime.AddDate(0, 0, 1) // 加一天
	expected := time.Date(2020, 3, 1, 0, 0, 0, 0, time.UTC)
	if !nextDay.Equal(expected) {
		t.Errorf("Expected %v, got %v", expected, nextDay)
	}

	// 测试时间截断
	truncateToHour := NewTime(time.Date(2020, 1, 1, 15, 30, 45, 123, time.UTC), false)
	truncatedTime := truncateToHour.Truncate(time.Hour)
	expectedTruncated := time.Date(2020, 1, 1, 15, 0, 0, 0, time.UTC)
	if !truncatedTime.Equal(expectedTruncated) {
		t.Errorf("Expected %v, got %v", expectedTruncated, truncatedTime)
	}

	// 测试时间舍入
	roundToMinute := NewTime(time.Date(2020, 1, 1, 15, 30, 45, 123, time.UTC), false)
	roundedTime := roundToMinute.Round(time.Minute)
	expectedRounded := time.Date(2020, 1, 1, 15, 31, 0, 0, time.UTC)
	if !roundedTime.Equal(expectedRounded) {
		t.Errorf("Expected %v, got %v", expectedRounded, roundedTime)
	}

	// 测试时区转换
	timeInUTC := NewTime(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), false)
	loc, _ := time.LoadLocation("America/New_York")
	timeInNY := timeInUTC.In(loc)
	expectedNYHour := 19 // UTC转换到纽约时间应该是前一天的晚上19点
	if timeInNY.Hour() != expectedNYHour {
		t.Errorf("Expected hour %d in New York, got %d", expectedNYHour, timeInNY.Hour())
	}
}

// TestTime_ErrorHandling 测试可能引发错误的场景
func TestTime_ErrorHandling(t *testing.T) {
	// 假设有一些方法可能在错误使用或异常情况下返回错误，例如UnmarshalJSON
	invalidJSON := []byte("this is not valid json")
	timeObj := NewTime(time.Now(), false)

	err := timeObj.UnmarshalJSON(invalidJSON)
	if err == nil {
		t.Errorf("Expected error when unmarshaling invalid JSON, got nil")
	}

	// 假设还有其他类似情况需要测试...
}

// TestTime_Parse 测试时间的解析功能
func TestTime_Parse(t *testing.T) {
	// 标准时间格式
	const timeFormat = "2006-01-02 15:04:05"

	// 正确的时间字符串
	timeString := "2023-01-02 14:00:00"
	expectedTime, _ := time.Parse(timeFormat, timeString)

	// 用时间解析方法来验证
	parsedTime, err := time.Parse(timeFormat, timeString)
	if err != nil {
		t.Errorf("Failed to parse time string '%s': %v", timeString, err)
	}
	if !parsedTime.Equal(expectedTime) {
		t.Errorf("Parsed time %v does not match expected time %v", parsedTime, expectedTime)
	}

	// 错误的时间字符串
	badTimeString := "not a real time"
	_, err = time.Parse(timeFormat, badTimeString)
	if err == nil {
		t.Errorf("Expected an error when parsing bad time string '%s'", badTimeString)
	}
}

// Package object 用于将float32类型的数据封装为线程安全的对象
package object

import (
	"math"        // 导入数学库，用于浮点数的位操作
	"strconv"     // 导入字符串转换库，用于浮点数与字符串之间的转换
	"sync/atomic" // 导入原子操作库，用于线程安全的操作
)

// Float32 接口定义了浮点数的基本操作
type Float32 interface {
	Set(value float32)         // 设置浮点数的值
	Get() float32              // 获取浮点数的值
	Add(delta float32) float32 // 增加浮点数的值
	String() string            // 将浮点数转换为字符串
}

// ThreadSafeFloat32 结构体提供了线程安全的浮点数操作
type ThreadSafeFloat32 struct {
	value uint32 // 浮点数存储为uint32类型，以便使用原子操作
}

// NewThreadSafeFloat32 函数创建一个新的线程安全的Float32实例
func NewThreadSafeFloat32(value float32) Float32 {
	return &ThreadSafeFloat32{value: math.Float32bits(value)} // 使用Float32bits将浮点数转换为位模式
}

// Set 方法使用原子操作安全地设置浮点数的值
func (tsf *ThreadSafeFloat32) Set(value float32) {
	atomic.StoreUint32(&tsf.value, math.Float32bits(value)) // 使用原子操作存储浮点数的位模式
}

// Get 方法使用原子操作安全地获取浮点数的值
func (tsf *ThreadSafeFloat32) Get() float32 {
	return math.Float32frombits(atomic.LoadUint32(&tsf.value)) // 使用原子操作读取位模式，并转换回浮点数
}

// Add 方法使用原子操作安全地增加浮点数的值
func (tsf *ThreadSafeFloat32) Add(delta float32) float32 {
	for { // 无限循环，直到操作成功
		oldValue := atomic.LoadUint32(&tsf.value)                            // 原子地读取当前值
		newValue := math.Float32bits(math.Float32frombits(oldValue) + delta) // 计算新值的位模式
		if atomic.CompareAndSwapUint32(&tsf.value, oldValue, newValue) {     // 尝试原子地进行值替换
			return math.Float32frombits(newValue) // 如果替换成功，返回新值
		}
		// 如果替换失败，则循环重试
	}
}

// String 方法将浮点数转换为字符串
func (tsf *ThreadSafeFloat32) String() string {
	return strconv.FormatFloat(float64(tsf.Get()), 'f', -1, 32) // 转换为64位浮点数后格式化为字符串
}

// NonThreadSafeFloat32 结构体提供非线程安全的浮点数操作
type NonThreadSafeFloat32 struct {
	value float32 // 直接存储为浮点数类型
}

// NewNonThreadSafeFloat32 函数创建一个新的非线程安全的Float32实例
func NewNonThreadSafeFloat32(value float32) Float32 {
	return &NonThreadSafeFloat32{value: value} // 直接赋值
}

// Set 方法直接设置浮点数的值
func (ntsf *NonThreadSafeFloat32) Set(value float32) {
	ntsf.value = value // 直接赋值
}

// Get 方法直接获取浮点数的值
func (ntsf *NonThreadSafeFloat32) Get() float32 {
	return ntsf.value // 直接返回
}

// Add 方法直接增加浮点数的值
func (ntsf *NonThreadSafeFloat32) Add(delta float32) float32 {
	ntsf.value += delta // 直接增加
	return ntsf.value   // 返回新值
}

// String 方法将浮点数转换为字符串
func (ntsf *NonThreadSafeFloat32) String() string {
	return strconv.FormatFloat(float64(ntsf.Get()), 'f', -1, 32) // 转换为64位浮点数后格式化为字符串
}

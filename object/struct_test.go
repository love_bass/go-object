package object

import (
	"reflect"
	"sync"
	"testing"
)

func TestStruct(t *testing.T) {
	// 创建新的 Struct 实例
	s := Struct[int]{Value: 42}

	// 测试 Get 方法
	if value := s.Get(); value != 42 {
		t.Errorf("Expected value %d, but got %d", 42, value)
	}

	// 测试 Set 方法
	s.Set(24)
	if value := s.Get(); value != 24 {
		t.Errorf("Expected value %d, but got %d", 24, value)
	}

	// 测试 Clone 方法
	clone, err := s.Clone()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	// 检查克隆的结构体是否具有相同的值
	if value := clone.Get(); value != 24 {
		t.Errorf("Expected cloned value %d, but got %d", 24, value)
	}

	// 检查克隆的结构是否为其他实例
	if reflect.ValueOf(&s).Pointer() == reflect.ValueOf(clone).Pointer() {
		t.Errorf("Expected cloned struct to be a different instance")
	}

	// 测试并发访问
	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()
		s.Set(10)
	}()

	go func() {
		defer wg.Done()
		value := s.Get()
		if value != 10 && value != 24 {
			t.Errorf("Expected value 10 or 24, but got %d", value)
		}
	}()

	wg.Wait()
}

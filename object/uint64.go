// Package object 用于将uint64类型的数据封装为线程安全的对象
package object

import (
	"strconv"     // 引入strconv包，用于类型转换
	"sync/atomic" // 引入sync/atomic包，用于实现原子操作
)

// UInt64 接口声明了一组操作无符号64位整型的方法
type UInt64 interface {
	Set(value uint64)        // 设置数值
	Get() uint64             // 获取数值
	Add(delta uint64) uint64 // 对数值进行原子增加操作
	String() string          // 获取数值的字符串表示形式
}

// ThreadSafeUInt64 结构体包含一个无符号64位整型数值，确保线程安全
type ThreadSafeUInt64 struct {
	value uint64 // 使用uint64类型存储数值
}

// NewThreadSafeUInt64 函数创建一个新的线程安全的无符号64位整型数值
func NewThreadSafeUInt64(value uint64) UInt64 {
	return &ThreadSafeUInt64{value: value} // 初始化结构体并返回
}

// Set 方法使用原子操作设置数值
func (tsi *ThreadSafeUInt64) Set(value uint64) {
	atomic.StoreUint64(&tsi.value, value) // 原子地存储数值
}

// Get 方法使用原子操作获取数值
func (tsi *ThreadSafeUInt64) Get() uint64 {
	return atomic.LoadUint64(&tsi.value) // 原子地加载数值
}

// Add 方法使用原子操作增加数值，并返回新值
func (tsi *ThreadSafeUInt64) Add(delta uint64) uint64 {
	return atomic.AddUint64(&tsi.value, delta) // 原子地增加数值并返回结果
}

// String 方法将数值转换为其十进制的字符串表示形式
func (tsi *ThreadSafeUInt64) String() string {
	return strconv.FormatInt(int64(tsi.Get()), 10) // 将数值转为int64后，转换为字符串
}

// NonThreadSafeUInt64 结构体包含一个无符号64位整型数值，非线程安全
type NonThreadSafeUInt64 struct {
	value uint64 // 直接使用uint64类型存储数值
}

// NewNonThreadSafeUInt64 函数创建一个新的非线程安全的无符号64位整型数值
func NewNonThreadSafeUInt64(value uint64) UInt64 {
	return &NonThreadSafeUInt64{value: value} // 初始化结构体并返回
}

// Set 方法直接设置数值，非线程安全
func (nts *NonThreadSafeUInt64) Set(value uint64) {
	nts.value = value // 直接赋值，存在竞态条件风险
}

// Get 方法直接获取数值，非线程安全
func (nts *NonThreadSafeUInt64) Get() uint64 {
	return nts.value // 直接返回数值
}

// Add 方法直接增加数值，并返回新值，非线程安全
func (nts *NonThreadSafeUInt64) Add(delta uint64) uint64 {
	nts.value += delta // 直接增加数值
	return nts.value   // 返回新的数值
}

// String 方法将数值转换为其十进制的字符串表示形式，非线程安全
func (nts *NonThreadSafeUInt64) String() string {
	return strconv.FormatInt(int64(nts.Get()), 10) // 将数值转为int64后，转换为字符串
}

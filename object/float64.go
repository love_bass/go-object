// Package object 用于将float64类型的数据封装为线程安全的对象
package object

import (
	"math"        // 导入数学库，用于浮点数的位操作
	"strconv"     // 导入字符串转换库，用于浮点数与字符串之间的转换
	"sync/atomic" // 导入原子操作库，用于线程安全的操作
)

// Float64 接口定义了浮点数的基本操作
type Float64 interface {
	Set(value float64)         // 设置浮点数的值
	Get() float64              // 获取浮点数的值
	Add(delta float64) float64 // 增加浮点数的值
	String() string            // 将浮点数转换为字符串
}

// ThreadSafeFloat64 结构体提供了线程安全的浮点数操作
type ThreadSafeFloat64 struct {
	value uint64 // 浮点数存储为uint64类型，以便使用原子操作
}

// NewThreadSafeFloat64 函数创建一个新的线程安全的Float64实例
func NewThreadSafeFloat64(value float64) Float64 {
	return &ThreadSafeFloat64{value: math.Float64bits(value)} // 使用Float64bits将浮点数转换为位模式
}

// Set 方法使用原子操作安全地设置浮点数的值
func (tsf *ThreadSafeFloat64) Set(value float64) {
	atomic.StoreUint64(&tsf.value, math.Float64bits(value)) // 使用原子操作存储浮点数的位模式
}

// Get 方法使用原子操作安全地获取浮点数的值
func (tsf *ThreadSafeFloat64) Get() float64 {
	return math.Float64frombits(atomic.LoadUint64(&tsf.value)) // 使用原子操作读取位模式，并转换回浮点数
}

// Add 方法使用原子操作安全地增加浮点数的值
func (tsf *ThreadSafeFloat64) Add(delta float64) float64 {
	for { // 无限循环，直到操作成功
		oldValue := atomic.LoadUint64(&tsf.value)                            // 原子地读取当前值
		newValue := math.Float64bits(math.Float64frombits(oldValue) + delta) // 计算新值的位模式
		if atomic.CompareAndSwapUint64(&tsf.value, oldValue, newValue) {     // 尝试原子地进行值替换
			return math.Float64frombits(newValue) // 如果替换成功，返回新值
		}
		// 如果替换失败，则循环重试
	}
}

// String 方法将浮点数转换为字符串
func (tsf *ThreadSafeFloat64) String() string {
	return strconv.FormatFloat(tsf.Get(), 'f', -1, 64) // 转换为64位浮点数后格式化为字符串
}

// NonThreadSafeFloat64 结构体提供非线程安全的浮点数操作
type NonThreadSafeFloat64 struct {
	value float64 // 直接存储为浮点数类型
}

// NewNonThreadSafeFloat64 函数创建一个新的非线程安全的Float64实例
func NewNonThreadSafeFloat64(value float64) Float64 {
	return &NonThreadSafeFloat64{value: value} // 直接赋值
}

// Set 方法直接设置浮点数的值
func (ntsf *NonThreadSafeFloat64) Set(value float64) {
	ntsf.value = value // 直接赋值
}

// Get 方法直接获取浮点数的值
func (ntsf *NonThreadSafeFloat64) Get() float64 {
	return ntsf.value // 直接返回
}

// Add 方法直接增加浮点数的值
func (ntsf *NonThreadSafeFloat64) Add(delta float64) float64 {
	ntsf.value += delta // 直接增加
	return ntsf.value   // 返回新值
}

// String 方法将浮点数转换为字符串
func (ntsf *NonThreadSafeFloat64) String() string {
	return strconv.FormatFloat(ntsf.Get(), 'f', -1, 64) // 转换为64位浮点数后格式化为字符串
}

package object

import "sync"

type Any[T any] struct {
	Value  T            // Value用于存储任意类型的值
	locker sync.RWMutex // locker用于读写锁
}

func (a *Any[T]) Set(value T) {
	a.locker.Lock()
	defer a.locker.Unlock()
	a.Value = value
}

func (a *Any[T]) Get() T {
	a.locker.RLock()
	defer a.locker.RUnlock()
	return a.Value
}

package object

import (
	"fmt"
	"testing"
)

// 测试并发安全场景
func TestConcurrentSafe(t *testing.T) {
	safeSlice := NewStringSlice([]string{"a", "b", "c"}, true)

	// 测试Set和Get
	safeSlice.Set([]string{"d", "e", "f"})
	if fmt.Sprint(safeSlice.Get()) != "[d e f]" {
		t.Error("Set and Get failed for concurrent safe slice")
	}

	// 测试Remove
	safeSlice.Remove("e", true)
	if fmt.Sprint(safeSlice.Get()) != "[d f]" {
		t.Error("Remove failed for concurrent safe slice")
	}

	// 测试Add
	safeSlice.Add("g")
	if fmt.Sprint(safeSlice.Get()) != "[d f g]" {
		t.Error("Add failed for concurrent safe slice")
	}

	// 测试Search
	searchResult := safeSlice.Search("f")
	if len(searchResult) != 1 || searchResult[1] != "f" {
		t.Error("Search failed for concurrent safe slice")
	}
}

// 测试非并发安全场景
func TestNonConcurrentSafe(t *testing.T) {
	unsafeSlice := NewStringSlice([]string{"x", "y", "z"}, false)

	// 测试Set和Get
	unsafeSlice.Set([]string{"m", "n", "o"})
	if fmt.Sprint(unsafeSlice.Get()) != "[m n o]" {
		t.Error("Set and Get failed for non-concurrent safe slice")
	}

	// 测试Remove
	unsafeSlice.Remove("n", true)
	if fmt.Sprint(unsafeSlice.Get()) != "[m o]" {
		t.Error("Remove failed for non-concurrent safe slice")
	}

	// 测试Add
	unsafeSlice.Add("p")
	if fmt.Sprint(unsafeSlice.Get()) != "[m o p]" {
		t.Error("Add failed for non-concurrent safe slice")
	}

	// 测试Contains
	if !unsafeSlice.Contains("o") {
		t.Error("Contains failed for non-concurrent safe slice")
	}
}

// 性能测试函数
func BenchmarkConcurrentSafe(b *testing.B) {
	slice := NewStringSlice(make([]string, 1000), true)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		slice.Add(fmt.Sprintf("item-%d", i))
		slice.Contains("item-500")
	}
}

func BenchmarkNonConcurrentSafe(b *testing.B) {
	slice := NewStringSlice(make([]string, 1000), false)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		slice.Add(fmt.Sprintf("item-%d", i))
		slice.Contains("item-500")
	}
}

package object

import (
	"math/rand"
	"sync"
	"testing"
)

func TestInt32(t *testing.T) {
	// 功能测试
	threadSafe := NewThreadSafeInt32(0)
	nonThreadSafe := NewNonThreadSafeInt32(0)

	// 设置值测试
	threadSafe.Set(100)
	nonThreadSafe.Set(100)
	if threadSafe.Get() != nonThreadSafe.Get() {
		t.Error("Set method does not work as expected")
	}

	// 增加值测试
	var delta int32 = 50
	threadSafe.Add(delta)
	nonThreadSafe.Add(delta)
	if threadSafe.Get() != nonThreadSafe.Get() {
		t.Error("Add method does not work as expected")
	}

	// 并发测试（线程安全）
	var wg sync.WaitGroup
	const concurrency = 100
	wg.Add(concurrency)
	for i := 0; i < concurrency; i++ {
		go func() {
			threadSafe.Add(rand.Int31n(100)) // 随机增加一个较小的值
			wg.Done()
		}()
	}
	wg.Wait()
	finalValueTS := threadSafe.Get()

	// 确保并发操作后，线程安全的值是正确的（无法预知准确值，只能验证不发生数据竞争）
	if finalValueTS <= 150 { // 考虑初始值、delta 和并发增加的随机值
		t.Errorf("ThreadSafeInt32 failed in concurrent scenario. Final value: %d", finalValueTS)
	}

	// 不进行并发测试（非线程安全），因为这会导致不可预测的结果
}

func TestThreadSafeInt32_Concurrency(t *testing.T) {
	tsi := NewThreadSafeInt32(0)
	var wg sync.WaitGroup
	const concurrency = 100
	wg.Add(concurrency)
	for i := 0; i < concurrency; i++ {
		go func(id int) {
			tsi.Add(int32(id))
			wg.Done()
		}(i)
	}
	wg.Wait()
	if got := tsi.Get(); got != int32(concurrency*(concurrency-1)/2) {
		t.Errorf("Expected sum of numbers from 0 to %d to be %d, but got %d", concurrency-1, concurrency*(concurrency-1)/2, got)
	}
}

func BenchmarkThreadSafeInt32(b *testing.B) {
	tsi := NewThreadSafeInt32(0)
	for n := 0; n < b.N; n++ {
		tsi.Add(1)
	}
}

func BenchmarkNonThreadSafeInt32(b *testing.B) {
	nts := NewNonThreadSafeInt32(0)
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			nts.Add(1)
		}
	})
}

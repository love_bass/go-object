package object

import (
	"bytes"        // 引入bytes包，用于操作字节
	"encoding/gob" // 引入gob包，用于对象的编码和解码
	"errors"       // 引入errors包，用于生成错误信息
	"sync"         // 引入sync包，用于同步操作，提供锁等同步工具
)

// Struct 是一个泛型结构体，提供并发控制
type Struct[T any] struct {
	Value  T
	locker sync.RWMutex
}

// Get 方法获取结构体的值
func (s *Struct[T]) Get() T {
	s.locker.RLock()         // 读锁
	defer s.locker.RUnlock() // 延迟读解锁
	return s.Value           // 返回结构体的值
}

// Set 方法设置结构体的值
func (s *Struct[T]) Set(value T) {
	s.locker.Lock()         // 加锁
	defer s.locker.Unlock() // 延迟解锁
	s.Value = value         // 设置结构体的值
}

// Clone 方法深度拷贝结构体
func (s *Struct[T]) Clone() (*Struct[T], error) {
	s.locker.Lock()                   // 加锁
	defer s.locker.Unlock()           // 延迟解锁
	buffer := new(bytes.Buffer)       // 创建一个新的bytes.Buffer
	encoder := gob.NewEncoder(buffer) // 创建一个新的编码器
	if err := encoder.Encode(s.Value); err != nil {
		return nil, errors.New("编码错误，存在复杂的数据结构，无法深度拷贝。") // 如果解码失败，返回错误
	}
	var dst T                         // 创建一个新的泛型结构体
	decoder := gob.NewDecoder(buffer) // 创建一个新的解码器
	if err := decoder.Decode(&dst); err != nil {
		return nil, errors.New("解码错误，存在复杂的数据结构，无法深度拷贝。") // 如果解码失败，返回错误
	}
	result := &Struct[T]{} // 创建一个新的泛型结构体实例
	result.Value = dst     // 设置克隆后的值
	return result, nil     // 返回新的结构体实例和nil
}

// Package object 用于将布尔值封装为并发安全的对象，提供一些基本的操作方法
package object

import "sync" // 导入sync包，包含同步原语，比如互斥锁

// Bool 类型包含一个布尔值和一个读写互斥锁
type Bool struct {
	Value  bool         // Value用于存储布尔值
	locker sync.RWMutex // locker是一个读写互斥锁，用于控制并发访问Value
}

// Set 方法设置Bool的Value
func (b *Bool) Set(value bool) {
	b.locker.Lock()         // 加锁，防止其他并发访问冲突
	defer b.locker.Unlock() // defer语句，确保方法结束时解锁
	b.Value = value         // 设置Value的值
}

// Get 方法返回Bool的Value
func (b *Bool) Get() bool {
	b.locker.RLock()         // 读锁，允许多个goroutine同时读取Value
	defer b.locker.RUnlock() // defer语句，确保方法结束时解锁
	return b.Value           // 返回Value的值
}

// Toggle 方法切换Bool的Value
func (b *Bool) Toggle() {
	b.locker.Lock()         // 加锁，防止其他并发访问冲突
	defer b.locker.Unlock() // defer语句，确保方法结束时解锁
	b.Value = !b.Value      // 切换Value的值
}

// IsTrue 方法检查Bool的Value是否为true
func (b *Bool) IsTrue() bool {
	b.locker.RLock()         // 读锁，允许多个goroutine同时读取Value
	defer b.locker.RUnlock() // defer语句，确保方法结束时解锁
	return b.Value           // 返回Value是否为true
}

// IsFalse 方法检查Bool的Value是否为false
func (b *Bool) IsFalse() bool {
	b.locker.RLock()         // 读锁，允许多个goroutine同时读取Value
	defer b.locker.RUnlock() // defer语句，确保方法结束时解锁
	return !b.Value          // 返回Value是否为false
}

// LogicalAnd 方法对Bool的Value执行逻辑与操作
func (b *Bool) LogicalAnd(value bool) {
	b.locker.Lock()            // 加锁，防止其他并发访问冲突
	defer b.locker.Unlock()    // defer语句，确保方法结束时解锁
	b.Value = b.Value && value // Value与传入的value执行逻辑与操作
}

// LogicalOr 方法对Bool的Value执行逻辑或操作
func (b *Bool) LogicalOr(value bool) {
	b.locker.Lock()            // 加锁，防止其他并发访问冲突
	defer b.locker.Unlock()    // defer语句，确保方法结束时解锁
	b.Value = b.Value || value // Value与传入的value执行逻辑或操作
}

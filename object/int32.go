// Package object 用于将int32类型的数据封装为线程安全的对象
package object

import (
	"strconv"     // 导入strconv包，用于整数与字符串之间的转换
	"sync/atomic" // 导入sync/atomic包，提供原子操作，用于线程安全的访问和修改数据
)

// Int32 接口定义了整数的一组操作，包括设置、获取、增加值以及转换为字符串
type Int32 interface {
	Set(value int32)       // 设置整数值
	Get() int32            // 获取当前整数值
	Add(delta int32) int32 // 增加值，并返回新值
	String() string        // 将整数转换为字符串
}

// ThreadSafeInt32 结构体提供一个线程安全的整数类型
type ThreadSafeInt32 struct {
	value int32 // 使用int32类型存储整数值，确保64位和32位系统兼容性
}

// NewThreadSafeInt32 函数创建并返回一个新的线程安全的Int32实例
func NewThreadSafeInt32(value int32) Int32 {
	return &ThreadSafeInt32{value: value} // 初始化时设置整数值
}

// Set 方法使用原子操作来安全地设置整数值
func (tsi *ThreadSafeInt32) Set(value int32) {
	atomic.StoreInt32(&tsi.value, value) // 原子地存储值，避免并发写入问题
}

// Get 方法使用原子操作来安全地获取当前整数值
func (tsi *ThreadSafeInt32) Get() int32 {
	return atomic.LoadInt32(&tsi.value) // 原子地读取值，确保在并发访问时的一致性
}

// Add 方法使用原子操作来安全地增加整数值，并返回新值
func (tsi *ThreadSafeInt32) Add(delta int32) int32 {
	return atomic.AddInt32(&tsi.value, delta) // 原子地增加值，返回操作后的新值
}

// String 方法返回当前整数值的字符串表示
func (tsi *ThreadSafeInt32) String() string {
	return strconv.FormatInt(int64(tsi.Get()), 10) // 将整数值转换为字符串，基数为10
}

// NonThreadSafeInt32 结构体提供一个非线程安全的整数类型
type NonThreadSafeInt32 struct {
	value int32 // 直接使用int32类型存储整数值
}

// NewNonThreadSafeInt32 函数创建并返回一个新的非线程安全的Int32实例
func NewNonThreadSafeInt32(value int32) Int32 {
	return &NonThreadSafeInt32{value: value} // 初始化时设置整数值
}

// Set 方法直接设置整数值，非线程安全
func (nts *NonThreadSafeInt32) Set(value int32) {
	nts.value = value // 直接赋值，存在并发访问风险
}

// Get 方法直接获取当前整数值，非线程安全
func (nts *NonThreadSafeInt32) Get() int32 {
	return nts.value // 直接返回值，存在并发访问风险
}

// Add 方法直接增加整数值，并返回新值，非线程安全
func (nts *NonThreadSafeInt32) Add(delta int32) int32 {
	nts.value += delta // 直接修改值，存在并发访问风险
	return nts.value   // 返回修改后的值
}

// String 方法返回当前整数值的字符串表示，非线程安全
func (nts *NonThreadSafeInt32) String() string {
	return strconv.FormatInt(int64(nts.Get()), 10) // 将整数值转换为字符串，基数为10
}

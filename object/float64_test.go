package object

import (
	"math"
	"math/rand"
	"sync"
	"testing"
)

func TestFloat64Interface(t *testing.T) {
	// 功能测试
	threadSafe := NewThreadSafeFloat64(10.5)
	nonThreadSafe := NewNonThreadSafeFloat64(10.5)

	// Set方法测试
	threadSafe.Set(20.7)
	nonThreadSafe.Set(20.7)
	if threadSafe.Get() != nonThreadSafe.Get() {
		t.Error("Set method does not work as expected")
	}

	// Add方法测试
	delta := 5.3
	threadSafe.Add(delta)
	nonThreadSafe.Add(delta)
	if threadSafe.Get() != nonThreadSafe.Get() {
		t.Error("Add method does not work as expected")
	}

	// 并发测试（线程安全）
	threadSafeConcurrent := NewThreadSafeFloat64(0)
	var wg sync.WaitGroup
	const concurrency = 100
	wg.Add(concurrency)
	for i := 0; i < concurrency; i++ {
		go func(delta float64) {
			threadSafeConcurrent.Add(delta)
			wg.Done()
		}(rand.Float64())
	}
	wg.Wait()
	_ = threadSafeConcurrent.Get() // 检查是否存在数据竞争，但无法直接验证结果

	// 不进行并发测试（非线程安全），因为这会导致不可预测的结果
}

func TestThreadSafeFloat64_Concurrency(t *testing.T) {
	tsf := NewThreadSafeFloat64(0.0)
	var wg sync.WaitGroup
	const concurrency = 100
	wg.Add(concurrency)

	for i := 0; i < concurrency; i++ {
		go func(id int) {
			tsf.Add(float64(id) * 0.1) // 将 id 转换为一个小的浮点数增量，避免过大导致溢出
			wg.Done()
		}(i)
	}
	wg.Wait()

	// 计算预期总和，这里假设每个 goroutine 添加的是 id * 0.1
	expectedSum := float64(concurrency*(concurrency-1)/2) * 0.1

	// 注意：浮点数运算可能存在精度误差，这里使用一个容忍范围来判断结果是否接近预期值
	epsilon := 0.0001
	if math.Abs(tsf.Get()-expectedSum) > epsilon {
		t.Errorf("Expected sum of numbers from 0.0 to %.1f to be approximately %.1f, but got %.1f", float64(concurrency-1)*0.1, expectedSum, tsf.Get())
	}
}

func BenchmarkThreadSafeFloat64(b *testing.B) {
	tsf := NewThreadSafeFloat64(0)
	for n := 0; n < b.N; n++ {
		tsf.Add(1.0)
	}
}

func BenchmarkNonThreadSafeFloat64(b *testing.B) {
	ntsf := NewNonThreadSafeFloat64(0)
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			ntsf.Add(1.0)
		}
	})
}

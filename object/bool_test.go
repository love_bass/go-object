package object

import (
	"sync"
	"testing"
	"time"
)

// TestBoolConcurrentSafety 测试Bool类型并发安全
func TestBoolConcurrentSafety(t *testing.T) {
	// 创建一个并发安全的布尔值实例
	boolObj := &Bool{}

	// 启动多个goroutine并发修改和读取Bool值
	var wg sync.WaitGroup
	for i := 0; i < 1000; i++ {
		wg.Add(2) // 每轮增加两个goroutine，一个写一个读

		go func(id int, isSetTrue bool) {
			defer wg.Done()
			boolObj.Set(isSetTrue)
			time.Sleep(time.Millisecond) // 引入随机延迟以增加并发冲突的可能性
		}(i, i%2 == 0)

		go func(id int) {
			defer wg.Done()
			_ = boolObj.Get()
			time.Sleep(time.Millisecond) // 同样引入延迟
		}(i)

		// 逻辑与、逻辑或测试
		if i%100 == 0 { // 每100次循环进行一次逻辑操作测试
			wg.Add(2)
			go func() {
				defer wg.Done()
				boolObj.LogicalAnd(true)
			}()
			go func() {
				defer wg.Done()
				boolObj.LogicalOr(false)
			}()
		}
	}

	wg.Wait() // 等待所有goroutine完成

	// 因为并发操作，我们无法预测最终Value的确切值，但测试主要验证并发操作的正确性而非具体值
	t.Logf("Final Value: %v", boolObj.Get())
}

// TestBoolMethods 测试Bool的各种方法
func TestBoolMethods(t *testing.T) {
	boolObj := &Bool{}

	// Test Set and Get
	boolObj.Set(true)
	if got := boolObj.Get(); !got {
		t.Error("Expected true, got false")
	}

	// Test Toggle
	boolObj.Toggle()
	if got := boolObj.Get(); got {
		t.Error("Expected false after toggle, got true")
	}

	// Test IsTrue and IsFalse
	boolObj.Set(true)
	if !boolObj.IsTrue() {
		t.Error("Expected IsTrue to return true")
	}
	if boolObj.IsFalse() {
		t.Error("Expected IsFalse to return false")
	}

	// Test LogicalAnd and LogicalOr
	boolObj.Value = false
	boolObj.LogicalAnd(false)
	if got := boolObj.Get(); got {
		t.Error("Expected false after LogicalAnd(false), got true")
	}
	boolObj.Value = false
	boolObj.LogicalOr(true)
	if !boolObj.Get() {
		t.Error("Expected true after LogicalOr(true), got false")
	}
}

package object

import (
	"math"
	"math/rand"
	"sync"
	"testing"
)

func TestFloat32Interface(t *testing.T) {
	// 功能测试
	threadSafe := NewThreadSafeFloat32(10.5)
	nonThreadSafe := NewNonThreadSafeFloat32(10.5)

	// Set方法测试
	threadSafe.Set(20.7)
	nonThreadSafe.Set(20.7)
	if threadSafe.Get() != nonThreadSafe.Get() {
		t.Error("Set method does not work as expected")
	}

	// Add方法测试
	var delta float32 = 5.3
	threadSafe.Add(delta)
	nonThreadSafe.Add(delta)
	if threadSafe.Get() != nonThreadSafe.Get() {
		t.Error("Add method does not work as expected")
	}

	// 并发测试（线程安全）
	threadSafeConcurrent := NewThreadSafeFloat32(0)
	var wg sync.WaitGroup
	const concurrency = 100
	wg.Add(concurrency)
	for i := 0; i < concurrency; i++ {
		go func(delta float32) {
			threadSafeConcurrent.Add(delta)
			wg.Done()
		}(rand.Float32())
	}
	wg.Wait()
	_ = threadSafeConcurrent.Get() // 检查是否存在数据竞争，但无法直接验证结果

	// 不进行并发测试（非线程安全），因为这会导致不可预测的结果
}

func TestThreadSafeFloat32_Concurrency(t *testing.T) {
	tsf := NewThreadSafeFloat32(0.0)
	var wg sync.WaitGroup
	const concurrency = 100
	wg.Add(concurrency)

	for i := 0; i < concurrency; i++ {
		go func(id int) {
			tsf.Add(float32(id) * 0.1) // 将 id 转换为一个小的浮点数增量，避免过大导致溢出
			wg.Done()
		}(i)
	}
	wg.Wait()

	// 计算预期总和，这里假设每个 goroutine 添加的是 id * 0.1
	expectedSum := float32(concurrency*(concurrency-1)/2) * 0.1

	// 注意：浮点数运算可能存在精度误差，这里使用一个容忍范围来判断结果是否接近预期值
	epsilon := 0.0001
	if math.Abs(float64(tsf.Get()-expectedSum)) > epsilon {
		t.Errorf("Expected sum of numbers from 0.0 to %.1f to be approximately %.1f, but got %.1f", float32(concurrency-1)*0.1, expectedSum, tsf.Get())
	}
}

func BenchmarkThreadSafeFloat32(b *testing.B) {
	tsf := NewThreadSafeFloat32(0)
	for n := 0; n < b.N; n++ {
		tsf.Add(1.0)
	}
}

func BenchmarkNonThreadSafeFloat32(b *testing.B) {
	ntsf := NewNonThreadSafeFloat32(0)
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			ntsf.Add(1.0)
		}
	})
}

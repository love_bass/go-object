package object

import (
	"fmt"
	"strconv"
	"testing"
	"time"
)

// TestStringA 测试线程安全的字符串实例
func TestStringA(t *testing.T) {
	// 测试线程安全的字符串实例

	// 创建一个线程安全的字符串实例
	// 第二个参数为true，表示线程安全
	threadSafeStr := NewString("Hello World", true)

	// 对线程安全实例进行操作
	go func() {
		// 在另一个goroutine中进行Concat操作，预计得到结果 Hello World, GPT!
		threadSafeStr.Concat(", GPT!")
	}()

	go func() {
		// 在另一个goroutine中进行Set操作，预计得到结果 Modified from another goroutine.
		threadSafeStr.Set("Modified from another goroutine.")
	}()

	// 等待两个goroutine完成操作
	time.Sleep(time.Millisecond * 100)

	// 验证Set操作的结果
	if !threadSafeStr.Equal("Modified from another goroutine.") {
		t.Errorf("Expected 'Modified from another goroutine.', got '%s'", threadSafeStr.Get())
	}

	// 验证Concat操作的结果，预计不包含', GPT!'，因为前面已经使用 Set 方法修改了字符串
	if threadSafeStr.Contains(", GPT!") {
		t.Errorf("Expected to not contain ', GPT!', but there is.")
	}

	// 测试非线程安全的字符串实例
	// 创建一个非线程安全的字符串实例
	// 第二个参数为false，表示非线程安全
	localUnsafeStr := NewString("Unsafe String", false)

	// 对非线程安全实例进行操作，通常不需要等待，因为它们不会相互阻塞
	// 预计得到结果 Unsafe String, this is unsafe.
	localUnsafeStr.Concat(", this is unsafe.")

	// 验证非线程安全实例的操作结果
	if localUnsafeStr.Get() != "Unsafe String, this is unsafe." {
		t.Errorf("Expected 'Unsafe String, this is unsafe.', got '%s'", localUnsafeStr.Get())
	}

	// 测试其他方法
	str := NewString("Test String", true)
	tests := []struct {
		name        string
		description string
		method      func(*String) interface{}
		expected    interface{}
	}{
		{
			"Count",
			"统计字符串中指定子串的个数",
			func(s *String) interface{} { return s.Count("t") },
			2,
		},
		{
			"HasPrefix",
			"判断字符串是否以指定前缀开头",
			func(s *String) interface{} { return s.HasPrefix("Test") },
			true,
		},
		{
			"HasSuffix",
			"判断字符串是否以指定后缀结尾",
			func(s *String) interface{} { return s.HasSuffix("String") },
			true,
		},
		{
			"Index",
			"获取字符串中指定子串的位置",
			func(s *String) interface{} { return s.Index("St") },
			5,
		},
		{
			"IsEmpty",
			"判断字符串是否为空",
			func(s *String) interface{} { return s.IsEmpty() },
			false,
		},
		{
			"Length",
			"获取字符串的长度",
			func(s *String) interface{} { return s.Length() },
			11,
		},
		{
			"ToLower",
			"将字符串转换为小写",
			func(s *String) interface{} { return s.ToLower() },
			"test string",
		},
		{
			"ToUpper",
			"将字符串转换为大写",
			func(s *String) interface{} { return s.ToUpper() },
			"TEST STRING",
		},
		{
			"Trim",
			"去除字符串中指定的前缀和后缀",
			func(s *String) interface{} { return s.Trim("Tes") },
			"t String",
		},
		{
			"TrimSpace",
			"去除字符串前后的空白字符",
			func(s *String) interface{} { return s.TrimSpace() },
			"Test String",
		},
		{
			"Split",
			"将字符串按指定分隔符分割成字符串数组",
			func(s *String) interface{} { return s.Split(" ") },
			[]string{"Test", "String"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			actual := tt.method(str)
			// 由于测试函数中有返回[]string，而切片不能直接拿来比较，所以说要进行一个类型断言
			if v, ok := tt.expected.([]string); ok {
				if !slicesAreEqual(v, tt.expected.([]string)) {
					fmt.Println(tt.description, "失败")
					t.Errorf("Expected %+v, got %+v", tt.expected, actual)
					return
				}
				fmt.Println(tt.description, "通过")
				return
			}

			if actual != tt.expected {
				fmt.Println(tt.description, "失败")
				t.Errorf("Expected %v, got %v", tt.expected, actual)
				return
			}
			fmt.Println(tt.description, "通过")
		})
	}
}

// TestStringB 测试线程安全的字符串实例
func TestStringB(t *testing.T) {
	tests := []struct {
		name        string
		description string
		value       string
		threadSafe  bool
	}{
		{"NonThreadSafeString", "非线程安全", "GoLang", false},
		{"ThreadSafeString", "线程安全", "GoLang", true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewString(tt.value, tt.threadSafe)

			// 测试 Concat 方法
			s.Concat(" Programming")
			if got := s.Get(); got != "GoLang Programming" {
				t.Errorf("Concat() = %v, want %v", got, "GoLang Programming")
			}

			// 测试 Count 方法
			if got := s.Count("g"); got != 3 {
				t.Errorf("Count() = %v, want %v", got, 3)
			}

			// 测试 Contains 方法
			if got := s.Contains("Lang"); !got {
				t.Errorf("Contains() = %v, want %v", got, true)
			}

			// 测试 Equal 方法
			if got := s.Equal("GoLang Programming"); !got {
				t.Errorf("Equal() = %v, want %v", got, true)
			}

			// 测试 EqualFold 方法
			if got := s.EqualFold("golang programming"); !got {
				t.Errorf("EqualFold() = %v, want %v", got, true)
			}

			// 测试 Fields 方法
			if got := len(s.Fields()); got != 2 {
				t.Errorf("Fields() = %v, want %v", got, 2)
			}

			// 测试 HasPrefix 方法
			if got := s.HasPrefix("Go"); !got {
				t.Errorf("HasPrefix() = %v, want %v", got, true)
			}

			// 测试 HasSuffix 方法
			if got := s.HasSuffix("ming"); !got {
				t.Errorf("HasSuffix() = %v, want %v", got, true)
			}

			// 测试 Index 方法
			if got := s.Index("Lang"); got != 2 {
				t.Errorf("Index() = %v, want %v", got, 2)
			}

			// 测试 IsEmpty 方法
			if got := s.IsEmpty(); got {
				t.Errorf("IsEmpty() = %v, want %v", got, false)
			}

			// 测试 Length 方法
			if got := s.Length(); got != 18 {
				t.Errorf("Length() = %v, want %v", got, 18)
			}

			// 测试 Replace 方法
			s.Set("GoLang Programming")
			if got := s.Replace("Lang", "pher", 1); got != "Gopher Programming" {
				t.Errorf("Replace() = %v, want %v", got, "Gopher Programming")
			}

			// 测试 Set 方法
			s.Set("NewStringSlice Value")
			if got := s.Get(); got != "NewStringSlice Value" {
				t.Errorf("Set() = %v, want %v", got, "NewStringSlice Value")
			}

			// 测试 Split 方法
			if got := s.Split(" "); !slicesAreEqual(got, []string{"NewStringSlice", "Value"}) {
				t.Errorf("Split() = %+v, want %+v", got, []string{"NewStringSlice", "Value"})
			}

			// 测试 ToLower 方法
			if got := s.ToLower(); got != "new value" {
				t.Errorf("ToLower() = %v, want %v", got, "new value")
			}

			// 测试 ToUpper 方法
			if got := s.ToUpper(); got != "NEW VALUE" {
				t.Errorf("ToUpper() = %v, want %v", got, "NEW VALUE")
			}

			// 测试 Trim 方法
			s.Set("   spaced   ")
			if got := s.Trim(" "); got != "spaced" {
				t.Errorf("Trim() = '%v', want '%v'", got, "spaced")
			}

			// 测试 TrimSpace 方法
			if got := s.TrimSpace(); got != "spaced" {
				t.Errorf("TrimSpace() = '%v', want '%v'", got, "spaced")
			}
		})
	}
}

// BenchmarkString 用于测试线程安全字符串的性能
func BenchmarkString(b *testing.B) {
	threadSafeStr := NewString("Benchmark String", true)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		threadSafeStr.Concat(" ")
		threadSafeStr.Concat("Benchmark")
		threadSafeStr.Concat("String")
		threadSafeStr.Concat(",")
		threadSafeStr.Concat(strconv.Itoa(i))
		threadSafeStr.TrimSpace()
	}
}

// 辅助函数，比较两个切片是否相等，需要遍历切片逐个元素地比较。
func slicesAreEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

// Package object 用于将uint32类型的数据封装为线程安全的对象
package object

import (
	"strconv"     // 引入strconv包，用于类型转换
	"sync/atomic" // 引入sync/atomic包，用于实现原子操作
)

// UInt32 接口声明了一组操作无符号32位整型的方法
type UInt32 interface {
	Set(value uint32)        // 设置数值
	Get() uint32             // 获取数值
	Add(delta uint32) uint32 // 对数值进行原子增加操作
	String() string          // 获取数值的字符串表示形式
}

// ThreadSafeUInt32 结构体包含一个无符号32位整型数值，确保线程安全
type ThreadSafeUInt32 struct {
	value uint32 // 使用uint32类型存储数值
}

// NewThreadSafeUInt32 函数创建一个新的线程安全的无符号32位整型数值
func NewThreadSafeUInt32(value uint32) UInt32 {
	return &ThreadSafeUInt32{value: value} // 初始化结构体并返回
}

// Set 方法使用原子操作设置数值
func (tsi *ThreadSafeUInt32) Set(value uint32) {
	atomic.StoreUint32(&tsi.value, value) // 原子地存储数值
}

// Get 方法使用原子操作获取数值
func (tsi *ThreadSafeUInt32) Get() uint32 {
	return atomic.LoadUint32(&tsi.value) // 原子地加载数值
}

// Add 方法使用原子操作增加数值，并返回新值
func (tsi *ThreadSafeUInt32) Add(delta uint32) uint32 {
	return atomic.AddUint32(&tsi.value, delta) // 原子地增加数值并返回结果
}

// String 方法将数值转换为其十进制的字符串表示形式
func (tsi *ThreadSafeUInt32) String() string {
	return strconv.FormatInt(int64(tsi.Get()), 10) // 将数值转为int64后，转换为字符串
}

// NonThreadSafeUInt32 结构体包含一个无符号32位整型数值，非线程安全
type NonThreadSafeUInt32 struct {
	value uint32 // 直接使用uint32类型存储数值
}

// NewNonThreadSafeUInt32 函数创建一个新的非线程安全的无符号32位整型数值
func NewNonThreadSafeUInt32(value uint32) UInt32 {
	return &NonThreadSafeUInt32{value: value} // 初始化结构体并返回
}

// Set 方法直接设置数值，非线程安全
func (nts *NonThreadSafeUInt32) Set(value uint32) {
	nts.value = value // 直接赋值，存在竞态条件风险
}

// Get 方法直接获取数值，非线程安全
func (nts *NonThreadSafeUInt32) Get() uint32 {
	return nts.value // 直接返回数值
}

// Add 方法直接增加数值，并返回新值，非线程安全
func (nts *NonThreadSafeUInt32) Add(delta uint32) uint32 {
	nts.value += delta // 直接增加数值
	return nts.value   // 返回新的数值
}

// String 方法将数值转换为其十进制的字符串表示形式，非线程安全
func (nts *NonThreadSafeUInt32) String() string {
	return strconv.FormatInt(int64(nts.Get()), 10) // 将数值转为int64后，转换为字符串
}

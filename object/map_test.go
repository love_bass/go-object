package object

import (
	"reflect"
	"testing"
)

// 测试NewMap函数，确保Map可以正确初始化
func TestNewMap(t *testing.T) {
	m := NewMap[int, string](true)
	if m == nil {
		t.Errorf("NewMap failed to create a new map instance")
	}
	if len(m.Value) != 0 {
		t.Errorf("New map should be empty")
	}
}

// 测试Add和Get方法
func TestAddAndGet(t *testing.T) {
	m := NewMap[int, string](true)
	m.Add(1, "test")
	val, ok := m.Get(1)
	if !ok || val != "test" {
		t.Errorf("Get() failed, expected 'test', got %s", val)
	}
}

// 测试Update方法
func TestUpdate(t *testing.T) {
	m := NewMap[int, string](true)
	m.Add(1, "test")
	m.Update(1, "updated")
	val, ok := m.Get(1)
	if !ok || val != "updated" {
		t.Errorf("Expected 'updated', got %s", val)
	}
}

// 测试Delete方法
func TestDelete(t *testing.T) {
	m := NewMap[int, string](true)
	m.Add(1, "test")
	m.Delete(1)
	val, ok := m.Get(1)
	if ok {
		t.Errorf("Expected key 1 to be deleted, but it's still present")
	}
	if val != "" {
		t.Errorf("Expected '', got %s", val)
	}
}

// 测试Clean方法
func TestClean(t *testing.T) {
	m := NewMap[int, string](true)
	m.Add(1, "test")
	m.Clean()
	if len(m.Value) != 0 {
		t.Errorf("Map should be empty after clean")
	}
}

// 测试Size方法
func TestSize(t *testing.T) {
	m := NewMap[int, string](true)
	m.Add(1, "test")
	size := m.Size()
	if size != 1 {
		t.Errorf("Expected size 1, got %d", size)
	}
}

// 测试Keys和Values方法
func TestKeysAndValues(t *testing.T) {
	m := NewMap[int, string](true)
	m.Add(1, "one")
	keys := m.Keys()
	if len(keys) != 1 || keys[0] != 1 {
		t.Errorf("Keys() failed, expected [1], got %v", keys)
	}
	values := m.Values()
	if len(values) != 1 || values[0] != "one" {
		t.Errorf("Values() failed, expected ['one'], got %v", values)
	}
}

// 测试Contains方法
func TestContains(t *testing.T) {
	m := NewMap[int, string](true)
	m.Add(1, "test")
	if !m.Contains(1) {
		t.Errorf("Map should contain key 1")
	}
	if m.Contains(2) {
		t.Errorf("Map should not contain key 2")
	}
}

// 测试IsEmpty方法
func TestIsEmpty(t *testing.T) {
	m := NewMap[int, string](true)
	if !m.IsEmpty() {
		t.Errorf("New map should be empty")
	}
	m.Add(1, "test")
	if m.IsEmpty() {
		t.Errorf("Map should not be empty after adding elements")
	}
}

// 测试Clone方法
func TestClone(t *testing.T) {
	m := NewMap[int, []string](true)
	m.Add(1, []string{"test", "found"})
	clone, err := m.Clone(true)
	if err != nil {
		t.Errorf("Clone() failed: %v", err)
	}
	if val, ok := clone.Value[1]; !ok || !reflect.DeepEqual(val, []string{"test", "found"}) {
		t.Errorf("Clone() failed to copy elements correctly")
	}
}

// 测试Merge方法
func TestMerge(t *testing.T) {
	m1 := NewMap[int, string](true)
	m1.Add(1, "one")
	m2 := NewMap[int, string](true)
	m2.Add(2, "two")
	m1.Merge(m2, false)
	if size := m1.Size(); size != 2 {
		t.Errorf("Expected size 2 after merge, got %d", size)
	}
}

// 测试Entries方法
func TestEntries(t *testing.T) {
	m := NewMap[int, string](true)
	m.Add(1, "one")
	entries := m.Entries()
	if len(entries) != 1 || entries[1] != "one" {
		t.Errorf("Entries() failed, expected map[1:one], got %v", entries)
	}
}

// 测试Equals方法
func TestEquals(t *testing.T) {
	m1 := NewMap[int, string](true)
	m1.Add(1, "one")
	m2 := NewMap[int, string](true)
	m2.Add(1, "one")
	equal, err := m1.Equals(m2)
	if err != nil || !equal {
		t.Errorf("Equals() failed, expected true, got %v, error: %v", equal, err)
	}
	m2.Add(2, "two")
	equal, err = m1.Equals(m2)
	if equal {
		t.Errorf("Equals() failed, expected false, got %v", equal)
	}
}
